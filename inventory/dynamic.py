#!/usr/bin/env python3

from os import uname
from json import dumps
from yaml import load
from glob import glob
from os.path import isfile

CONFIG_DIR = '/etc/ansible/roles'

sysname, nodename, _, _, _ = uname ()
sysname = sysname.lower ().strip ()

inventory = {
    '_meta': {'hostvars': {nodename: {'sysname': sysname, 'nodename': nodename}}},
    'all': {'hosts': [nodename], 'vars': {}, 'children': []},
}

enable_roles = []
disable_roles = []
configured_roles = []
all_roles = sorted ([r.split ('/')[2] for r in glob ('./roles/*/tasks/main.yml')])

for role_name in all_roles:
    cfgfile = "%s/%s.yml" % (CONFIG_DIR, role_name)
    if isfile (cfgfile):
        with open (cfgfile, 'r') as fh:
            cfg = load (fh.read ())
            fh.close ()
        inventory[role_name] = {'hosts': [nodename], 'vars': {}, 'children': []}
        kn = "%s_enable" % role_name
        if cfg.get (kn, False) and role_name != sysname and role_name != 'common':
            enable_roles.append (role_name)
            inventory['_meta']['hostvars'][nodename].update (cfg)
        elif role_name == sysname or role_name == 'common':
            # sysname and common roles are always ran, so no need to enable them
            inventory['_meta']['hostvars'][nodename].update (cfg)
        else:
            disable_roles.append (role_name)
            inventory['_meta']['hostvars'][nodename][kn] = False
        configured_roles.append (role_name)
    else:
        inventory[role_name] = {'hosts': [], 'vars': {}, 'children': []}

# sysname matching and common roles are always enabled
for rn in ['common', sysname]:
    if not nodename in inventory[rn]['hosts']:
        inventory[rn]['hosts'].append (nodename)

inventory['_meta']['hostvars'][nodename]['all_roles'] = all_roles
inventory['_meta']['hostvars'][nodename]['configured_roles'] = sorted (configured_roles)
inventory['_meta']['hostvars'][nodename]['disable_roles'] = sorted (disable_roles)
inventory['_meta']['hostvars'][nodename]['enable_roles'] = sorted (enable_roles)

print (dumps (inventory, indent = 4, sort_keys = True))
