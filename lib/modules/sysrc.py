#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2018, Jeremías Casteglione <jrmsdev@gmail.com>

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community',
}

DOCUMENTATION = '''
---
module: sysrc
short_description: manage system configuration files (/etc/rc.conf and others).
description:
    - This module manipulates system configuration files (rc.conf and others) using C(sysrc) OS command.
version_added: "2.6.0"
options:
    name:
        description:
            - Configuration option name.
        required: true
    value:
        description:
            - Desired value of the configuration option.
    file:
        description:
            - Configuration file path.
author: "Jeremías Casteglione <jrmsdev@gmail.com>"
'''

EXAMPLES = '''
# Get option value
- sysrc:
    name: firewall_enable

# Set option value
- sysrc:
    name: sshd_enable
    value: YES
    file: /etc/rc.conf.local
'''

# ~ import subprocess
from ansible.module_utils.basic import AnsibleModule

class SysrcModule (object):

    def __init__ (self, module):
        self.module = module
        self.args = self.module.params
        self.sysrc_cmd = self.module.get_bin_path ('sysrc', required = True)
        self.changed = False
        self.value = ''
        self.main ()

    def main (self):
        cval = self.get_val ()
        if self.args['value'] != '' and cval != self.args['value']:
            self.changed = True
            if not self.module.check_mode:
                self.set_val ()

    def get_val (self):
        cmd = [self.sysrc_cmd, '-n',]
        if self.args['file']:
            cmd.extend (['-f', self.args['file']])
        cmd.append (self.args['name'])
        rc, out, err = self.module.run_command (cmd, check_rc = True)
        if err:
            self.module.fail_json (rc = rc, stdout = out, msg = err)
        self.value = out.strip ()
        return self.value

    def set_val (self):
        cmd = [self.sysrc_cmd]
        if self.args['file']:
            cmd.extend (['-f', self.args['file']])
        cmd.append ('{}={}'.format (self.args['name'], self.args['value']))
        rc, out, err = self.module.run_command (cmd, check_rc = True)
        if err:
            self.module.fail_json (rc = rc, stdout = out, msg = err)
        self.value = self.args['value']

def main ():
    module = AnsibleModule (
        argument_spec = dict (
            name = dict (required = True, type = 'str'),
            value = dict (required = False, type = 'str'),
            file = dict (required = False, type = 'path'),
        ),
        supports_check_mode = True,
    )

    if module.params['name'] is None:
        module.fail_json (msg = "name cannot be None")

    if module.params['name'] == '':
        module.fail_json (msg = "name cannot be blank")

    module.params['name'] == module.params['name'].strip ()

    if module.params['value'] is None:
        module.params['value'] = ''

    module.params['value'] == module.params['value'].strip ()

    # ugly(?) hack to fix ansible converting YES to True and NO to False strings
    if module.params['value'] == 'True':
        module.params['value'] = 'YES'
    elif module.params['value'] == 'False':
        module.params['value'] = 'NO'

    if module.params['file'] is None:
        module.params['file'] = ''

    module.params['file'] == module.params['file'].strip ()

    result = SysrcModule (module)

    kn = module.params['name'].strip ()
    module.exit_json (**{
        'changed': result.changed,
        kn: result.value,
    })

if __name__ == '__main__':
    main ()
