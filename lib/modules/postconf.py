#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2018, Jeremías Casteglione <jrmsdev@gmail.com>

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community',
}

DOCUMENTATION = '''
---
module: postconf
short_description: manage postfix configuration
description:
    - This module manipulates postfix configuration using C(postconf).
version_added: "2.6.0"
options:
    name:
        description:
            - Configuration key.
        required: true
    value:
        description:
            - Desired value of the key.
    config_dir:
        description:
            - Configuration directory path for main.cf file.
author: "Jeremías Casteglione <jrmsdev@gmail.com>"
'''

EXAMPLES = '''
# Get main.cf value
- postconf:
    name: config_directory

# Set main.cf value
- postconf:
    name: inet_interfaces
    value: all
    config_dir: /usr/local/etc/postfix
'''

from ansible.module_utils.basic import AnsibleModule

class PostconfModule (object):

    def __init__ (self, module):
        self.module = module
        self.args = dict ()
        for k,v in self.module.params.items ():
            self.args[k] = v.strip()
        self.postconf_cmd = self.module.get_bin_path ('postconf', required = True)
        self.changed = False
        self.value = ''
        self.main ()

    def main (self):
        cval = self.get_val ()
        if self.args['value'] != '' and cval != self.args['value']:
            self.changed = True
            if not self.module.check_mode:
                self.set_val ()

    def get_val (self):
        cmd = [self.postconf_cmd, '-h',]
        if self.args['config_dir']:
            cmd.extend (['-c', self.args['config_dir']])
        cmd.append (self.args['name'])
        rc, out, err = self.module.run_command (cmd, check_rc = True)
        if err:
            self.module.fail_json (rc = rc, stdout = out, msg = err)
        self.value = out.strip ()
        return self.value

    def set_val (self):
        cmd = [self.postconf_cmd]
        if self.args['config_dir']:
            cmd.extend (['-c', self.args['config_dir']])
        cmd.append ('{}={}'.format (self.args['name'], self.args['value']))
        rc, out, err = self.module.run_command (cmd, check_rc = True)
        if err:
            self.module.fail_json (rc = rc, stdout = out, msg = err)
        self.value = self.args['value']

def main ():
    module = AnsibleModule (
        argument_spec = dict (
            name = dict (required = True, type = 'str'),
            value = dict (required = False, type = 'str'),
            config_dir = dict (required = False, type = 'path'),
        ),
        supports_check_mode = True,
    )

    if module.params['name'] is None:
        module.fail_json (msg = "name cannot be None")
    if module.params['name'] == '':
        module.fail_json (msg = "name cannot be blank")

    if module.params['value'] is None:
        module.params['value'] = ''

    if module.params['config_dir'] is None:
        module.params['config_dir'] = ''

    result = PostconfModule (module)

    kn = module.params['name'].strip ()
    module.exit_json (**{
        'changed': result.changed,
        kn: result.value,
    })

if __name__ == '__main__':
    main ()
