#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# (c) 2018, Jeremías Casteglione <jrmsdev@gmail.com>

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community',
}

DOCUMENTATION = '''
---
module: fstab
short_description: manage fstab entries
description:
    - This module manages fstab entries.
version_added: "2.6.0"
options:
    device:
        description:
            - Device name or path.
        required: true
    path:
        description:
            - Path for the mount point.
        required: true
    fstype:
        description:
            - Filesystem type.
        required: true
    options:
        description:
            - Filesystem mount options.
        required: true
    dump:
        description:
            - Dump filesystem?
        default: 0
        choices: [0, 1]
    pass:
        description:
            - Pass number for the run in which the filesystem should be checked.
        default: 0
        choices: [0, 1, 2]
    backup:
        description:
            - Backup file before to modify it.
        default: 'no'
        choices: ['yes', 'no']
    state:
        description:
            - Should the entry be present or absent?.
        default: present
        choices: [absent, present]
author: "Jeremías Casteglione <jrmsdev@gmail.com>"
'''

EXAMPLES = '''
# Root filesystem
- fstab:
    device: /dev/ada0p1
    path: /
    fstype: ufs
    options: rw
    dump: 1
    pass: 1

# Swap device
- fstab:
    device: /dev/ada0p2
    path: none
    fstype: swap
    options: sw
    backup: yes

# Remove Linux proc filesystem
- fstab:
    device: proc
    path: /proc
    fstype: procfs
    options: rw
    state: absent
'''

from os import fdopen, unlink
from os.path import isfile
from tempfile import mkstemp
from ansible.module_utils.basic import AnsibleModule


FSTAB_FILE = '/etc/fstab'


class FstabEntry (object):

    def __init__ (self, args):
        self.device = args.get ('device', None)
        self.path = args.get ('path', None)
        self.fstype = args.get ('fstype', None)
        self.options = args.get ('options', None)
        self.dump = args.get ('dump', None)
        self.passno = args.get ('pass', None)
        self.remove = False

    def __eq__ (self, other):
        return self.values () == other.values ()

    def __ne__ (self, other):
        return not self.__eq__ (other)

    def __str__ (self):
        return '{}\t{}\t{}\t{}\t{} {}'.format (*self.values ())

    def __repr__ (self):
        return '<FstabEntry%s>' % str (self.values ())

    def ID (self):
        return "%s:%s" % (self.path, self.device)

    def values (self):
        return (self.device, self.path, self.fstype, self.options, self.dump, self.passno)

    def load (self, values):
        self.device = values[0]
        self.path = values[1]
        self.fstype = values[2]
        self.options = values[3]
        self.dump = values[4]
        self.passno = values[5]


class FstabModule (object):

    def __init__ (self, module):
        self.module = module
        self.args = dict ()
        for k,v in self.module.params.items ():
            if isinstance (v, int):
                self.args[k] = str(v)
            elif isinstance (v, bool):
                self.args[k] = v
            else:
                self.args[k] = v.strip()
        self.changed = False
        self.diff = {
            'before': '',
            'after': '',
            'before_header': '%s' % FSTAB_FILE,
            'after_header': '%s' % FSTAB_FILE,
        }
        self.src_lines = list ()
        self.dst_lines = list ()
        self.main ()

    def main (self):
        self.check_entry (FstabEntry (self.args))
        if self.module._diff:
            self.diff['before'] = ''.join (self.src_lines)
            self.diff['after'] = ''.join (self.dst_lines)
        if self.changed and not self.module.check_mode:
            (fd, fn) = mkstemp (prefix = 'ansible.', suffix = '.fstab')
            try:
                with fdopen (fd, 'w') as fh:
                    fh.writelines (self.dst_lines)
                    fh.close ()
                if self.args['backup']:
                    self.module.backup_local (FSTAB_FILE)
                self.module.atomic_move (fn, FSTAB_FILE)
            except Exception as e:
                if isfile (fn):
                    unlink (fn)
                raise e

    def check_entry (self, new_e):
        with open (FSTAB_FILE, 'r') as src:
            e_found = False
            for line in [l.strip() for l in src.readlines ()]:
                self.src_lines.append ("%s\n" % line)
                if line == '' or line.startswith ('#'):
                    self.dst_lines.append ("%s\n" % line)
                else:
                    file_e = FstabEntry ({})
                    file_e.load (line.split ())
                    if file_e.ID () == new_e.ID ():
                        e_found = True
                        if self.args['state'] == 'absent':
                            self.changed = True
                            # we do not save it to dst_lines
                        else:
                            if file_e != new_e:
                                self.changed = True
                                self.dst_lines.append ("%s\n" % str (new_e))
                    else:
                        self.dst_lines.append ("%s\n" % str (file_e))
            if not e_found:
                self.changed = True
                self.dst_lines.append ("%s\n" % str (new_e))
            src.close ()


def main ():
    args = dict (
        device = dict (required = True, type = 'str'),
        path = dict (required = True, type = 'str'),
        fstype = dict (required = True, type = 'str'),
        options = dict (required = True, type = 'str'),
        dump = dict (type = 'int', choices = [0, 1], default = 0),
        backup = dict (type = 'bool', default = False),
        state = dict (type = 'str', default = 'present', choices = ['absent', 'present']),
    )
    args['pass'] = dict (type = 'int', choices = [0, 1, 2], default = 0)
    module = AnsibleModule (
        argument_spec = args,
        supports_check_mode = True,
    )
    result = FstabModule (module)
    module.exit_json (changed = result.changed, diff = result.diff)


if __name__ == '__main__':
    main ()
